package com.socialgroup.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SocialGroupConfigApplication {

	public static void main(final String[] args) {
		SpringApplication.run(SocialGroupConfigApplication.class, args);
	}

}
